
# awesome.dev on VM with php 5.6 / Ubuntu 14.04

## Installation

    git clone git@gitlab.com:mattanddev/awesome.dev.git
    vagrant up

see /puphpet/config.yaml for configuration. If you edit anything run ```vagrant provision``` to activate.

## hosts

add ```30.0.0.30 awesome.dev``` to your hostfile


## Email testing

You can catch emails running http://30.0.0.30:1080/ before testing.

## DB Management

[Adminer (phpmyadmin replacement)](http://30.0.0.30/adminer/)

***Connect via Navicat / Sequel pro***

Connect using SSH tunnel, username vagrant and SSH key generated at puphpet/files/dot/ssh/id_rsa. Password for key is also vagrant. This key is generated after your initial $ vagrant up!